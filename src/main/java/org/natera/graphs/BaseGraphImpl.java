package org.natera.graphs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class BaseGraphImpl<T, V extends Graph.Vertex<T>, E extends Graph.Edge<V>> extends AbstractGraph<T, E> {

    private final VertexCreator<T, V> vertexCreator;
    private final EdgeCreator<V, E> edgeCreator;
    private final GraphStorage<V, E> graphStorage;
    private final GraphDataRetriever<V, E> graphDataRetriever;

    BaseGraphImpl(VertexCreator<T, V> vertexCreator, EdgeCreator<V, E> edgeCreator, GraphStorage<V, E> graphStorage) {
        this(vertexCreator, edgeCreator, graphStorage, new SimpleGraphDataRetriever<>(graphStorage));
    }

    BaseGraphImpl(
            VertexCreator<T, V> vertexCreator,
            EdgeCreator<V, E> edgeCreator,
            GraphStorage<V, E> graphStorage,
            GraphDataRetriever<V, E> graphDataRetriever) {

        this.vertexCreator = vertexCreator;
        this.edgeCreator = edgeCreator;
        this.graphStorage = graphStorage;
        this.graphDataRetriever = graphDataRetriever;
    }

    @Override
    public void addVertex(T vertex) {
        checkNotNull(vertex);
        graphStorage.addVertex(vertexCreator.create(vertex));
    }

    @Override
    public void addEdge(T from, T to) {
        checkNotNull(from, to);
        graphStorage.addEdge(edgeCreator.create(vertexCreator.create(from), vertexCreator.create(to)));
    }

    @Override
    public List<T> getPath(T from, T to) {
        checkNotNull(from, to);
        return graphDataRetriever.getPath(vertexCreator.create(from), vertexCreator.create(to))
                .stream()
                .map(Vertex::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public List<E> getPathOfEdges(T from, T to) {
        checkNotNull(from, to);
        List<V> path = graphDataRetriever.getPath(vertexCreator.create(from), vertexCreator.create(to));

        List<E> res = new ArrayList<>();
        for (int i = 1; i < path.size(); i++) {
            res.add(edgeCreator.create(path.get(i - 1), path.get(i)));
        }

        return res;
    }

    @Override
    public Collection<T> getAllVertices() {
        return graphStorage.getAllVertices().stream().map(Vertex::getValue).collect(Collectors.toList());
    }

    @Override
    public Collection<E> getAllEdges() {
        return graphStorage.getAllEdges();
    }

    @Override
    public boolean isDirected() {
        return graphStorage.isDirected();
    }

    private void checkNotNull(Object ... objects) {
        for (Object object : objects) {
            Objects.requireNonNull(object);
        }
    }

    interface VertexCreator<T, V> {
        V create(T userDefinedObj);
    }

    interface EdgeCreator<V, E> {
        E create(V source, V dest);
    }
}
