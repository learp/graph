package org.natera.graphs;

public interface ReversibleEdge<T> extends Graph.Edge<T> {
    <V extends ReversibleEdge<T>> V reverse();
}
