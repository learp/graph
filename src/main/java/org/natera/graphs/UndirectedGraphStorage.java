package org.natera.graphs;

import java.util.*;
import java.util.stream.Collectors;

public class UndirectedGraphStorage<V extends Graph.Vertex, E extends ReversibleEdge<V>> implements GraphStorage<V, E> {

    private final Map<V, Connections<V, E>> outgoingEdges;

    public UndirectedGraphStorage() {
        this(new HashMap<>());
    }

    UndirectedGraphStorage(Map<V, Connections<V, E>> outgoingEdges) {
        this.outgoingEdges = outgoingEdges;
    }

    @Override
    public List<E> getAllEdges() {
        return outgoingEdges.values()
                .stream()
                .map(Connections::getOriginal)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public List<V> getAllVertices() {
        return new ArrayList<>(outgoingEdges.keySet());
    }

    @Override
    public Collection<E> getOutgoingEdgesFrom(V vertex) {
        Connections<V, E> connections = outgoingEdges.get(vertex);
        return connections == null ? Collections.emptyList() : connections.getSources();
    }

    @Override
    public void addVertex(V vertex) {
        outgoingEdges.putIfAbsent(vertex, new Connections<>());
    }

    @Override
    public void addEdge(E ed) {
        Connections<V, E> edgesFromSource = outgoingEdges.get(ed.getSource());
        Connections<V, E> edgesFromDest = outgoingEdges.get(ed.getDestination());

        if (edgesFromSource == null || edgesFromDest == null) {
            throw new VertexNotFoundException();
        }

        edgesFromSource.add(ed, ed.getSource());
        edgesFromDest.add(ed, ed.getDestination());
    }

    @Override
    public boolean isDirected() {
        return false;
    }

    private class Connections<VRT, EDG extends ReversibleEdge<VRT>> {
        private final Collection<EDG> original = new ArrayList<>();
        private final Collection<EDG> sources = new ArrayList<>();

        public Collection<EDG> getSources() {
            return sources;
        }

        public Collection<EDG> getOriginal() {
            return original;
        }

        public void add(EDG edge, VRT source) {
            original.add(edge);
            if (edge.getSource().equals(source)) {
                sources.add(edge);
            } else {
                sources.add(edge.reverse());
            }
        }
    }
}
