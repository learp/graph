package org.natera.graphs;

import java.util.Objects;

public class DefaultVertex<T> implements Graph.Vertex<T> {
    private final T val;

    public DefaultVertex(T val) {
        this.val = val;
    }

    @Override
    public T getValue() {
        return val;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        DefaultVertex<?> that = (DefaultVertex<?>) other;
        return Objects.equals(val, that.val);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }

    @Override
    public String toString() {
        return String.valueOf(val);
    }
}
