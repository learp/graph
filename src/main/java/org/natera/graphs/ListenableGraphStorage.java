package org.natera.graphs;

public interface ListenableGraphStorage<V, E> extends GraphStorage<V, E> {
    void addListener(Listener<V, E> listener);

    interface Listener<V, E> {
        void onVertexAdded(V vertex);
        void onEdgeAdded(E edge);
    }
}
