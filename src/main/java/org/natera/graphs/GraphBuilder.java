package org.natera.graphs;

import org.natera.graphs.BaseGraphImpl.EdgeCreator;
import org.natera.graphs.BaseGraphImpl.VertexCreator;
import org.natera.graphs.Graph.Vertex;

import java.util.function.BiFunction;

public class GraphBuilder<T> {

    private BiFunction<Vertex<T>, Vertex<T>, Double> weightFunction = (source, dest) -> 1.0d;
    private final VertexCreator<T, Vertex<T>> vertexCreator = DefaultVertex::new;
    private final EdgeCreator<Vertex<T>, DefaultWeightedEdge<Vertex<T>>> edgeCreator = (source, dest) -> new DefaultWeightedEdge<>(source, dest, weightFunction);

    private final boolean directed;
    private boolean threadsafe = false;

    private GraphBuilder(boolean directed) {
        this.directed = directed;
    }

    public static <T> GraphBuilder<T> directed() {
        return new GraphBuilder<>(true);
    }

    public static <T> GraphBuilder<T> undirected() {
        return new GraphBuilder<>(false);
    }

    public GraphBuilder<T> threadsafe(boolean threadsafe) {
        this.threadsafe = threadsafe;
        return this;
    }

    public GraphBuilder<T> withWeightFunction(BiFunction<T, T, Double> weightFunction) {
        this.weightFunction = (source, dest) -> weightFunction.apply(source.getValue(), dest.getValue());
        return this;
    }

    public Graph<T, DefaultWeightedEdge<Vertex<T>>> build() {
        GraphStorage<Vertex<T>, DefaultWeightedEdge<Vertex<T>>> graphStorage;

        if (threadsafe) {
            if (directed) {
                graphStorage = new ConcurrentDirectedGraphStorage<>();
            } else {
                throw new UnsupportedOperationException();
            }
        } else {
            graphStorage = directed ? new DirectedGraphStorage<>() : new UndirectedGraphStorage<>();
        }

        return new BaseGraphImpl<>(vertexCreator, edgeCreator, graphStorage);
    }
}
