package org.natera.graphs;

import java.util.*;
import java.util.stream.Collectors;

public class DirectedGraphStorage<V extends Graph.Vertex, E extends Graph.Edge<V>> implements GraphStorage<V, E> {

    private final Map<V, List<E>> outgoingEdges;

    public DirectedGraphStorage() {
        this(new HashMap<>());
    }

    DirectedGraphStorage(Map<V, List<E>> outgoingEdges) {
        this.outgoingEdges = outgoingEdges;
    }

    @Override
    public List<E> getAllEdges() {
        return outgoingEdges.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Override
    public List<V> getAllVertices() {
        return new ArrayList<>(outgoingEdges.keySet());
    }

    @Override
    public List<E> getOutgoingEdgesFrom(V vertex) {
        return outgoingEdges.get(vertex);
    }

    @Override
    public void addVertex(V vertex) {
        outgoingEdges.putIfAbsent(vertex, new ArrayList<>());
    }

    @Override
    public void addEdge(E ed) {
        List<E> edgesFromSource = outgoingEdges.get(ed.getSource());
        List<E> edgesFromDest = outgoingEdges.get(ed.getDestination());

        if (edgesFromSource == null || edgesFromDest == null) {
            throw new VertexNotFoundException();
        }

        edgesFromSource.add(ed);
    }

    @Override
    public boolean isDirected() {
        return true;
    }
}
