package org.natera.graphs;

import java.util.Collection;

public interface GraphStorage<V, E> {
    Collection<E> getAllEdges();
    Collection<V> getAllVertices();

    Collection<E> getOutgoingEdgesFrom(V vertex);

    void addVertex(V vertex);
    void addEdge(E ed);

    boolean isDirected();
}
