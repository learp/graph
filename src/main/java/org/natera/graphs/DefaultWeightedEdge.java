package org.natera.graphs;

import java.util.Objects;
import java.util.function.BiFunction;

public class DefaultWeightedEdge<V> implements WeighedEdge<V>, ReversibleEdge<V> {

    private final BiFunction<V, V, Double> weightFunction;
    private final V source;
    private final V dest;

    public DefaultWeightedEdge(V source, V dest, BiFunction<V, V, Double> weightFunction) {
        this.weightFunction = weightFunction;
        this.source = source;
        this.dest = dest;
    }

    @Override
    public double getWeight() {
        return weightFunction.apply(source, dest);
    }

    @Override
    public V getSource() {
        return source;
    }

    @Override
    public V getDestination() {
        return dest;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends ReversibleEdge<V>> T reverse() {
        return (T) new DefaultWeightedEdge<>(dest, source, weightFunction);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultWeightedEdge<?> that = (DefaultWeightedEdge<?>) o;
        return Objects.equals(weightFunction, that.weightFunction) &&
                Objects.equals(source, that.source) &&
                Objects.equals(dest, that.dest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weightFunction, source, dest);
    }

    @Override
    public String toString() {
        return getSource() + " -> " + getDestination();
    }
}
