package org.natera.graphs;

import java.util.Collection;
import java.util.List;

/**
 * An interface for graph.
 *
 * @param <T> user defined type
 * @param <E> type of edges
 */
public interface Graph<T, E> {

    /**
     * Adds vertex to the graph if it's not present.
     * @param vertex - vertex to add
     */
    void addVertex(T vertex);

    /**
     * Adds edge to the graph.
     * @param from - first vertex
     * @param to - second vertex
     */
    void addEdge(T from, T to);

    /**
     * Returns path between two vertices.
     * @param from - vertex where path starts
     * @param to - vertex where path ends
     * @return list of vertices
     */
    List<T> getPath(T from, T to);

    /**
     * Returns path between two vertices.
     * @param from - vertex where path starts
     * @param to - vertex where path ends
     * @return list of edges
     */
    List<E> getPathOfEdges(T from, T to);

    /**
     * Returns all vertices.
     * @return collection of vertices
     */
    Collection<T> getAllVertices();

    /**
     * Returns all edges.
     * @return collection of edges
     */
    Collection<E> getAllEdges();

    /**
     * Returns whether the graph is directed.
     * @return true if graph directed, false otherwise.
     */
    boolean isDirected();

    /**
     * An interface for vertex.
     * @param <T> user defined type
     */
    interface Vertex<T> {

        /**
         * Returns vertex value.
         * @return object of user defined type
         */
        T getValue();
    }

    /**
     * An interface for edge.
     * @param <V> - vertex type
     */
    interface Edge<V> {

        /**
         * Returns edge source.
         * @return source vertex
         */
        V getSource();

        /**
         * Returns edge destination.
         * @return destination vertex
         */
        V getDestination();
    }
}
