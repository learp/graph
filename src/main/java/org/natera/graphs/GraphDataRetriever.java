package org.natera.graphs;

import java.util.List;

public interface GraphDataRetriever<V, E>  {
    List<V> getPath(V from, V to);
}
