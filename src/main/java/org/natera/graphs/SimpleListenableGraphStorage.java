package org.natera.graphs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SimpleListenableGraphStorage<V, E> implements ListenableGraphStorage<V, E> {

    private final GraphStorage<V, E> realGraphStorage;
    protected final List<Listener<V, E>> listeners = new ArrayList<>();

    public SimpleListenableGraphStorage(GraphStorage<V, E> realGraphStorage) {
        this.realGraphStorage = realGraphStorage;
    }

    @Override
    public Collection<E> getAllEdges() {
        return realGraphStorage.getAllEdges();
    }

    @Override
    public Collection<V> getAllVertices() {
        return realGraphStorage.getAllVertices();
    }

    @Override
    public Collection<E> getOutgoingEdgesFrom(V vertex) {
        return realGraphStorage.getOutgoingEdgesFrom(vertex);
    }

    @Override
    public void addVertex(V vertex) {
        realGraphStorage.addVertex(vertex);
        listeners.forEach(l -> l.onVertexAdded(vertex));
    }

    @Override
    public void addEdge(E edge) {
        realGraphStorage.addEdge(edge);
        listeners.forEach(l -> l.onEdgeAdded(edge));
    }

    @Override
    public void addListener(Listener<V, E> listener) {
        listeners.add(listener);
    }

    @Override
    public boolean isDirected() {
        return realGraphStorage.isDirected();
    }
}
