package org.natera.graphs;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

public class ConcurrentDirectedGraphStorage<V extends Graph.Vertex, E extends Graph.Edge<V>> implements GraphStorage<V, E> {

    private final Map<V, Queue<E>> outgoingEdges = new ConcurrentHashMap<>();

    @Override
    public List<E> getAllEdges() {
        return outgoingEdges.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Override
    public List<V> getAllVertices() {
        return new ArrayList<>(outgoingEdges.keySet());
    }

    @Override
    public Queue<E> getOutgoingEdgesFrom(V vertex) {
        return outgoingEdges.get(vertex);
    }

    @Override
    public void addVertex(V vertex) {
        outgoingEdges.putIfAbsent(vertex, new ConcurrentLinkedQueue<>());
    }

    @Override
    public void addEdge(E ed) {
        Queue<E> edgesFromSource = outgoingEdges.get(ed.getSource());
        Queue<E> edgesFromDest = outgoingEdges.get(ed.getDestination());

        if (edgesFromSource == null || edgesFromDest == null) {
            throw new VertexNotFoundException();
        }

        edgesFromSource.add(ed);
    }

    @Override
    public boolean isDirected() {
        return true;
    }
}
