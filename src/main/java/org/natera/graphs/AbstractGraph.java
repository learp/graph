package org.natera.graphs;

public abstract class AbstractGraph<T, E> implements Graph<T, E> {

    @Override
    public String toString() {
        return "Directed: " + isDirected() + ". Vertices: " + getAllVertices().toString();
    }
}
