package org.natera.graphs;

public interface WeighedEdge<V> extends Graph.Edge<V> {
    double getWeight();
}
