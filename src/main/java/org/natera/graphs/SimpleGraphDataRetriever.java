package org.natera.graphs;

import java.util.*;

public class SimpleGraphDataRetriever<V extends Graph.Vertex, E extends Graph.Edge<V>> implements GraphDataRetriever<V, E> {

    private final GraphStorage<V, E> graphStorage;

    SimpleGraphDataRetriever(GraphStorage<V, E> graphStorage) {
        this.graphStorage = graphStorage;
    }

    @Override
    public List<V> getPath(V from, V to) {
        Queue<V> queue = new LinkedList<>();
        Set<V> visited = new HashSet<>();
        Map<V, V> prevVertex = new HashMap<>();
        visited.add(from);
        queue.add(from);

        while (!queue.isEmpty()) {
            V currentVertex = queue.remove();

            if (currentVertex.equals(to)) {
                return getPath(prevVertex, to);
            }

            Collection<E> outgoingEdges = graphStorage.getOutgoingEdgesFrom(currentVertex);

            if (outgoingEdges == null || outgoingEdges.size() == 0) {
                continue;
            }

            outgoingEdges.forEach(edge -> {
                V targetVertex = edge.getDestination();

                if (!visited.contains(targetVertex)) {
                    visited.add(targetVertex);
                    queue.add(targetVertex);
                    prevVertex.put(targetVertex, currentVertex);
                }
            });
        }

        return new ArrayList<>();
    }

    private List<V> getPath(Map<V, V> prevVertex, V end) {
        V cur = end;
        List<V> res = new ArrayList<>();

        while (cur != null) {
            res.add(cur);
            cur = prevVertex.get(cur);
        }

        Collections.reverse(res);
        return res;
    }
}
