package org.natera.graphs.utils;

import org.natera.graphs.Graph;
import org.natera.graphs.UserDefinedType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class GraphGenerators {

    public static List<UserDefinedType> createChain(Graph<UserDefinedType, ?> graph, int vertexCount) {
        UserDefinedType[] userDefinedObjs = new UserDefinedType[vertexCount];

        userDefinedObjs[0] = new UserDefinedType( "0");
        graph.addVertex(userDefinedObjs[0]);
        for (int i = 1; i < vertexCount; i++) {
            userDefinedObjs[i] = new UserDefinedType(String.valueOf(i));
            graph.addVertex(userDefinedObjs[i]);
            graph.addEdge(userDefinedObjs[i - 1], userDefinedObjs[i]);
        }

        return new ArrayList<>(Arrays.asList(userDefinedObjs));
    }

    public static List<UserDefinedType> createCompleteGraph(Graph<UserDefinedType, ?> graph, int vertexCount) throws InterruptedException {
        return createCompleteGraph(graph, vertexCount, 1, true).getKey();
    }

    public static Pair<List<UserDefinedType>, List<Future<?>>> createCompleteGraph(Graph<UserDefinedType, ?> graph, int vertexCount, int threadCount, boolean waitUntilCreated) throws InterruptedException {
        List<UserDefinedType> userDefinedObjs = new ArrayList<>();

        for (int i = 0; i < vertexCount; i++) {
            UserDefinedType curUser = new UserDefinedType(String.valueOf(i));
            userDefinedObjs.add(curUser);
            graph.addVertex(curUser);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        List<Future<?>> futures = new ArrayList<>();
        for (int i = 0; i < vertexCount; i++) {
            final int k = i;
            futures.add(executorService.submit(() -> {
                UserDefinedType user_i = new UserDefinedType(String.valueOf(k));

                for (int j = 0; j < k; j++) {
                    UserDefinedType user_j = new UserDefinedType(String.valueOf(j));
                    graph.addEdge(user_j, user_i);
                    if (graph.isDirected()) {
                        graph.addEdge(user_i, user_j);
                    }
                }
            }));
        }

        if (waitUntilCreated) {
            executorService.shutdown();
            executorService.awaitTermination(100, TimeUnit.SECONDS);
        }

        return new Pair<>(userDefinedObjs, futures);
    }

    public static class Pair<K, V> {
        private final K key;
        private final V val;

        Pair(K key, V val) {
            this.key = key;
            this.val = val;
        }

        public K getKey() {
            return key;
        }

        public V getVal() {
            return val;
        }
    }
}
