package org.natera.graphs;

import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.natera.graphs.utils.GraphGenerators;
import org.natera.graphs.utils.GraphGenerators.Pair;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComplexGraphTest {

    private static Stream<Graph> graphProvider() {
        return Stream.of(
                GraphBuilder.directed().threadsafe(true).build()
                // GraphBuilder.undirected().threadsafe(true).build() todo
        );
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    @Timeout(60)
    public void canFindPathAndModifyGraphSimultaneously(Graph<UserDefinedType, ?> graph) throws InterruptedException, ExecutionException {
        Pair<List<UserDefinedType>, List<Future<?>>> pair = GraphGenerators.createCompleteGraph(graph, 3_000, 100, false);
        List<UserDefinedType> vertices = pair.getKey();
        ExecutorService executorService = constantlyReadFromGraph(graph);

        UserDefinedType from = vertices.get(0);
        UserDefinedType to = vertices.get(vertices.size() - 1);

        List<UserDefinedType> path = graph.getPath(from, to);

        while (path.isEmpty()) {
            path = graph.getPath(from, to);
        }

        executorService.shutdownNow();

        assertEquals(from, path.get(0));
        assertEquals(to, path.get(path.size() - 1));
        assertEquals(vertices.size(), graph.getAllVertices().size());

        List<Future<?>> futures = pair.getVal();
        for (Future<?> future : futures) {
            future.get();
        }

        assertEquals(vertices.size() * (vertices.size() - 1), graph.getAllEdges().size());
    }

    private ExecutorService constantlyReadFromGraph(Graph<UserDefinedType, ?> graph) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                graph.getAllVertices();
                graph.getAllEdges();
            }
        });

        return executorService;
    }
}
