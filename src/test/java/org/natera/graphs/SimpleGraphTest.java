package org.natera.graphs;

import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.natera.graphs.Graph.Edge;
import org.natera.graphs.Graph.Vertex;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.natera.graphs.utils.GraphGenerators.createChain;
import static org.natera.graphs.utils.GraphGenerators.createCompleteGraph;

public class SimpleGraphTest {

    private static Stream<Graph> graphProvider() {
        return Stream.of(
                GraphBuilder.directed().build(),
                GraphBuilder.directed().threadsafe(true).build(),

                GraphBuilder.undirected().build()
                // GraphBuilder.undirected().threadsafe(true).build() todo
        );
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void canAddVertexToGraph(Graph<UserDefinedType, ?> graph) {
        UserDefinedType userDefinedObj = new UserDefinedType("1");
        graph.addVertex(userDefinedObj);

        assertTrue(graph.getAllVertices().contains(userDefinedObj));
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void additionOfExistingVertexDoesntChangeGraph(Graph<UserDefinedType, ?> graph) {
        UserDefinedType userDefinedObj = new UserDefinedType("1");

        graph.addVertex(userDefinedObj);
        assertTrue(graph.getAllVertices().contains(userDefinedObj));

        graph.addVertex(userDefinedObj);
        assertEquals(1, graph.getAllVertices().size());
        assertTrue(graph.getAllVertices().contains(userDefinedObj));
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void cantAddNullVertexToGraph(Graph<UserDefinedType, ?> graph) {
        assertThrows(NullPointerException.class, () -> graph.addVertex(null));
    }
    
    @ParameterizedTest
    @MethodSource("graphProvider")
    void canAddEdgeToGraph(Graph<UserDefinedType, Edge<Vertex<UserDefinedType>>> graph) {
        UserDefinedType userDefinedObj_1 = new UserDefinedType("1");
        UserDefinedType userDefinedObj_2 = new UserDefinedType("2");

        graph.addVertex(userDefinedObj_1);
        graph.addVertex(userDefinedObj_2);
        graph.addEdge(userDefinedObj_1, userDefinedObj_2);

        assertEquals(1, graph.getAllEdges().size());
        Edge<Vertex<UserDefinedType>> edge = graph.getAllEdges().iterator().next();
        assertEquals(userDefinedObj_1, edge.getSource().getValue());
        assertEquals(userDefinedObj_2, edge.getDestination().getValue());
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void cantAddEdgeToGraphWhenVertexNotFound(Graph<UserDefinedType, ?> graph) {
        UserDefinedType userDefinedObj_1 = new UserDefinedType("1");
        UserDefinedType userDefinedObj_2 = new UserDefinedType("2");

        Executable addEdge = () -> graph.addEdge(userDefinedObj_1, userDefinedObj_2);
        assertThrows(VertexNotFoundException.class, addEdge);

        graph.addVertex(userDefinedObj_2);
        assertThrows(VertexNotFoundException.class, addEdge);
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void cantAddNullEdgeToGraph(Graph<UserDefinedType, ?> graph) {
        UserDefinedType userDefinedType = new UserDefinedType("1");

        assertThrows(NullPointerException.class, () -> graph.addEdge(null, null));
        assertThrows(NullPointerException.class, () -> graph.addEdge(null, userDefinedType));
        assertThrows(NullPointerException.class, () -> graph.addEdge(userDefinedType, null));
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void npeWhenFindingPathBetweenNulls(Graph<UserDefinedType, ?> graph) {
        assertThrows(NullPointerException.class, () -> graph.getPath(null, null));
        assertThrows(NullPointerException.class, () -> graph.addEdge(null, new UserDefinedType("1")));
        assertThrows(NullPointerException.class, () -> graph.addEdge(new UserDefinedType("1"), null));
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void emptyListIfPathNotFound(Graph<UserDefinedType, ?> graph) {
        UserDefinedType userDefinedObj_1 = new UserDefinedType("1");
        UserDefinedType userDefinedObj_2 = new UserDefinedType("2");

        graph.addVertex(userDefinedObj_1);
        graph.addVertex(userDefinedObj_2);

        List<UserDefinedType> path = graph.getPath(userDefinedObj_1, userDefinedObj_2);
        assertTrue(path.isEmpty());
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void emptyListIfVerticesNotFound(Graph<UserDefinedType, ?> graph) {
        List<UserDefinedType> path = graph.getPath(new UserDefinedType("1"), new UserDefinedType("2"));
        assertTrue(path.isEmpty());
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void canFindPathInGraph(Graph<UserDefinedType, Edge<Vertex<UserDefinedType>>> graph) {
        int vertexCount = 100;
        List<UserDefinedType> userDefinedObjs = createChain(graph, vertexCount);

        UserDefinedType from = userDefinedObjs.get(0);
        UserDefinedType to = userDefinedObjs.get(vertexCount - 1);

        List<UserDefinedType> path = graph.getPath(from, to);
        assertEquals(userDefinedObjs, path);

        List<Edge<Vertex<UserDefinedType>>> pathFromEdges = graph.getPathOfEdges(from, to);
        assertEquals(vertexCount - 1, pathFromEdges.size());

        for (int i = 0; i < pathFromEdges.size(); i++) {
            Edge<? extends Vertex<UserDefinedType>> edge = pathFromEdges.get(i);
            assertEquals(userDefinedObjs.get(i), edge.getSource().getValue());
            assertEquals(userDefinedObjs.get(i + 1), edge.getDestination().getValue());
        }
    }

    @ParameterizedTest
    @MethodSource("graphProvider")
    void canFindPathInCompleteGraph(Graph<UserDefinedType, Edge<Vertex<UserDefinedType>>> graph) throws InterruptedException {
        int vertexCount = 100;
        List<UserDefinedType> userDefinedObjs = createCompleteGraph(graph, vertexCount);

        UserDefinedType from = userDefinedObjs.get(0);
        UserDefinedType to = userDefinedObjs.get(vertexCount - 1);

        List<UserDefinedType> path = graph.getPath(from, to);
        assertEquals(2, path.size());
        assertEquals(from, path.get(0));
        assertEquals(to, path.get(1));

        List<? extends Edge<? extends Vertex<UserDefinedType>>> pathFromEdges = graph.getPathOfEdges(from, to);
        assertEquals(1, pathFromEdges.size());
        assertEquals(from, pathFromEdges.get(0).getSource().getValue());
        assertEquals(to, pathFromEdges.get(0).getDestination().getValue());
    }
}
